<div class="container">
                <div class="no-gutter row">           
        <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">  
            
            <div class="well well-sm">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <img src="http://placehold.it/380x500" alt="" class="img-rounded img-responsive" />
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <!-- Contact Info -->
                        <h4>Yael Sprikut</h4>
                        <p><small><cite title="San Francisco, USA">San Francisco, USA <i class="glyphicon glyphicon-map-marker">
                        </i></cite></small></p>
                        <p>
                        <i class="glyphicon glyphicon-envelope"></i>&nbsp;email@example.com
                        <br /></p>
                        <p><i class="glyphicon glyphicon-globe"></i><a href="http://www.jquery2dotnet.com">&nbsp;www.jquery2dotnet.com</a>
                        <br /></p>
                        <p><i class="glyphicon glyphicon-gift"></i>&nbsp;June 02, 1988</p>
                        <!-- Split button -->
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-envelope"></i>&nbsp;Send Message</button>
                        </div>
                    </div>
                </div> 
                <?php include 'profile/userbar.php';?>
                </div>
            </div><!---end of col-xs-12 col-sm-6 col-md-6--->
        </div><!---end of row--->
    </div><!---end of no-gutter row--->
</div><!---end of container--->