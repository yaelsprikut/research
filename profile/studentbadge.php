                <div class="row">
                    <div class="col-sm-6 col-md-4">
                         <img src="http://placehold.it/380x500" alt="" class="img-rounded img-responsive" />
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <!-- Contact Info -->
                        <h3><b>Yael Sprikut</b></h3>
                        <p><small><cite title="Toronto, ON">Toronto, ON <i class="glyphicon glyphicon-map-marker">
                        </i></cite></small></p>
                        <p>
                        <i class="glyphicon glyphicon-envelope"></i>&nbsp;ysprikut@georgebrown.ca
                        <br /></p>
                        <p><i class="glyphicon glyphicon-book"></i>&nbsp;T127 - Computer Programming Analyst
                        <br /></p>
                        <p><i class="glyphicon glyphicon-gift"></i>&nbsp;September 29, 1991</p>
                        <!-- Split button -->
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-envelope"></i>&nbsp;Send Message</button>
                        </div>
                    </div>
                </div> 