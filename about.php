<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>GBC Research Monster</title>
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet">
                <link rel="shortcut icon" href="images/rmfavicon.png" type="image/x-icon" />

		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="css/styles.css" rel="stylesheet">
	</head>
	<body>
            <!-- Begin Navbar -->
            <?php 
            session_start();
            include 'session/login.php';
            if(isset($_SESSION['login_user'])){
                include 'includes/navbar.php';
            }else{
                include 'index/navbar.php';       
            }          
            ?>
            <!-- Begin Header -->
            <?php include 'includes/header.php';?>



<!-- Begin Body -->
<div class="container">
	<div class="no-gutter row">
            <?php include 'includes/sidebar.php';?>            
      		<!-- Mid Bar -->
                <!-- right content column-->
      		<div class="col-md-10" id="content">
            	<div class="panel">
    			<div class="panel-heading" style="background-color:#555;color:#eee;">About Us</div>   
              	<div class="panel-body">
                    <div class="container">
                <div class="no-gutter row">           
        <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-7">  

                  <div class="well"> 
                    <h3>About Us</h3>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat 
                    nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia 
                    deserunt mollit anim id est laborum.
                  </div>

            </div><!---end of col-xs-12 col-sm-6 col-md-6--->
            <!---RSS FEED -->
            <?php include 'extras/ads.php';?>
        </div><!---end of row--->
    </div><!---end of no-gutter row--->            
</div><!---end of container--->   
            
             	
            </div><!--/panel-body-->
          </div><!--/panel-->
        </div><!--/end right column-->



</div>
</div>
<?php include 'includes/footer.php';?>
<!-- script references -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/scripts.js"></script>
	</body>
</html>